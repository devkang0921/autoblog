# 한번에 설치 pip install gspread oauth2client requests pyjwt datetime
import gspread # pip install gspread
from oauth2client.service_account import ServiceAccountCredentials # pip install oauth2client
import requests # pip install requests
import jwt	# pip install pyjwt
from datetime import datetime as date # pip install datetime
import config
import function

#구글시트연동 | json key파일로 인증
scope = [
'https://spreadsheets.google.com/feeds',
'https://www.googleapis.com/auth/drive',
]
json_file_name = config.spreadsheets_key
credentials = ServiceAccountCredentials.from_json_keyfile_name(json_file_name, scope)
gc = gspread.authorize(credentials)

# 구글시트 문서 가져오기 
doc = gc.open_by_url(config.spreadsheet_url)

# 시트 선택하기
worksheet = doc.worksheet(config.select_sheet)

# 범위(셀 위치 리스트) 가져오기
range_list = worksheet.range('A:A')

# 범위에서 각 셀 값 가져오기
for i in range(2, len(range_list)):
	cell = str(worksheet.acell('B'+str(i)).value)
	# 51번째 행이 넘어가면 실행되지 않음.
	if i > 51: exit('51번째 행까지 실행이 완료되었습니다')
	if range_list[i-1].value == "FALSE": 
		if len(cell) > 0 and cell != 'None':
			print("B" + str(i) +": 작업 진행중")
			
			# 선택한 시트에 따라서 작업할 데이터 불러오기
			row_data = worksheet.row_values(i)
			if row_data[5] == "":
				print("#ERROR!로 확인되어 업로드되지 않습니다.")
				# 완료 체크(체크되면 블로그로 업로드하지 않음)
				worksheet.update_acell('A'+str(i), 'TRUE')
				continue
			
			if config.select_sheet == '블로그자동화(en)':
				title_origin = row_data[1]
				title = row_data[2]
				body = row_data[4]
				tag = row_data[7]
				feature_image_url = row_data[9]
			else:
				title = row_data[1]
				body = row_data[3]
				tag = row_data[5]
				feature_image_url = row_data[7]


			print("==========블로그 자동 업로드==========")	

			# 최종이미지 URL 얻기
			response = requests.get(feature_image_url)
			if response.status_code == 200:
				feature_image_url = response.url
			else:
				print("Error:", response.status_code)

			# 본문에 html이 없으면 강제로 넣어주기	
			if function.contains_html_tags(body) == False:
				body = function.str_to_html(body)	

				
			# ghost Admin API key가져오기
			key = config.ghost_key
			id, secret = key.split(':')
			iat = int(date.now().timestamp())

			header = {'alg': 'HS256', 'typ': 'JWT', 'kid': id}
			payload = {
				'iat': iat,
				'exp': iat + 5 * 60,
				'aud': '/admin/'
			}

			# 토큰 생성
			token = jwt.encode(payload, bytes.fromhex(secret), algorithm='HS256', headers=header)

			# 인증 후 블로그 업로드
			url = config.domain + '/ghost/api/admin/posts/?source=html'
			headers = {'Authorization': 'Ghost {}'.format(token)}
			body = {'posts': [
						{"title": title,
						"html": body,
	   					"feature_image": feature_image_url,
						"status": "published",
						"visibility": "public", 
						 "tags": [
								{
									"name": tag
								}
							],   
		     			}]}
			r = requests.post(url, json=body, headers=headers)
			print(title_origin)
			print(title)
			print(r)

			# 완료 체크(체크되면 블로그로 업로드하지 않음)
			worksheet.update_acell('A'+str(i), 'TRUE')
			print("==========블로그 업로드 완료==========\n")	
		else:
			print("B"+str(i) + " | 미완료 | " + cell)
	else:
		print("B"+str(i) + " | 완료 | " + cell)
				
	