import re # 정규식사용을 위한 내장모듈로 설치할 필요없음.
from html import escape # 정규식사용을 위한 내장모듈로 설치할 필요없음.

# html이 포함되었는지 확인하는 코드
def contains_html_tags(text):
	pattern = re.compile(r"<.*?>")
	return bool(pattern.search(text))

# plaintext를 html로 변경해주는 코드
def str_to_html(text):
	lines = text.strip().split('\n')
	
	html = "<!DOCTYPE html>\n<html>\n<head>\n<title></title>\n</head>\n<body>\n"
	
	in_list = False
	
	for line in lines:
		if line.startswith('- '):
			if not in_list:
				html += "<ul>\n"
				in_list = True
			html += f"<li>{escape(line[2:])}</li>\n"
		else:
			if in_list:
				html += "</ul>\n"
				in_list = False
			html += f"<p>{escape(line)}</p>\n"
	
	if in_list:
		html += "</ul>\n"
	
	html += "</body>\n</html>"
	
	return html    