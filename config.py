# config
# 모든 API key는 절대 외부유출되지 않도록 주의하세요!

#도메인주소
domain = 'https://apujoos.com'
#구글시트 json키 파일 
spreadsheets_key = 'key.json' # 이 파일은 샘플용입니다. 실제로는 작동하지 않습니다.
#구글시트 url
spreadsheet_url = '사본으로 만들었던 스프레드시트의 URL을 입력해주세요'
#선택된 시트 이름
select_sheet = '블로그자동화(en)'
#ghost 어드민 키
ghost_key = 'Admin API key를 입력해주세요'
